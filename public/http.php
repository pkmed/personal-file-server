<?php

use App\Kernel;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Workerman\Connection\TcpConnection;
use Workerman\Protocols\Http\Request as WorkermanRequest;
use Workerman\Protocols\Http\Response;
use Workerman\Worker;

$http_worker = new Worker($_ENV['HTTP_URL']);

$http_worker->count = 1;

$http_worker->onMessage = static function (TcpConnection $connection, WorkermanRequest $data) use (&$kernel) {
    $_GET                      = $data->get();
    $_POST                     = $data->post();
    $_COOKIE                   = $data->cookie();
    $_FILES                    = $data->file();
    $_SESSION                  = $data->session();
    $_SERVER['REQUEST_URI']    = $data->uri();
    $_SERVER['REQUEST_METHOD'] = $data->method();

    if (preg_match("#^/(css|js)/[\w\-]+\.(css|js)$#", $_SERVER['REQUEST_URI'])) {
        $resp = fetchStaticFiles($data);
    } else {
        $resp = processSymfonyRoute($data, $kernel);
    }

    $connection->send($resp);
};

function processSymfonyRoute(WorkermanRequest $data, Kernel $kernel): Response
{
    /** @var array $headers */
    $headers = $data->header();
    $request = SymfonyRequest::createFromGlobals();
    $request->headers->add($headers);

    $response = $kernel->handle($request);

    $kernel->terminate($request, $response);

    return new Response(
        $response->getStatusCode(),
        $response->headers->allPreserveCase(),
        $response->getContent()
    );
}

function fetchStaticFiles(WorkermanRequest $data): Response
{
    $jsContentTypeHeader  = 'application/javascript';
    $cssContentTypeHeader = 'text/css; charset=UTF-8';

    $filename = __DIR__.$data->uri();
    if (file_exists($filename)) {
        $file    = file_get_contents($filename);
        $headers = [
            'Host'           => $_ENV['HTTP_URL'],
            'Date'           => gmdate('D, d M Y H:i:s T'),
            'Connection'     => 'close',
            'Content-Length' => strlen($file),
        ];
        $ext     = pathinfo($filename, PATHINFO_EXTENSION);
        if ($ext === 'js') {
            $headers['Content-Type'] = $jsContentTypeHeader;
        } else {
            $headers['Content-Type'] = $cssContentTypeHeader;
        }

        return new Response(200, $headers, $file);
    }

    return new Response(404, $data->header(), 'File not found');
}
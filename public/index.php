<?php

require dirname(__DIR__) . '/vendor/autoload.php';

use App\Kernel;
use App\PFS\CloudStorageBundle\Security\WSTokenHandler;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\ErrorHandler\Debug;
use Workerman\Worker;

(new Dotenv())->bootEnv(dirname(__DIR__) . '/.env');

if ($_SERVER['APP_DEBUG']) {
    umask(0000);

    Debug::enable();
}


$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$kernel->boot();
WSTokenHandler::setCacheDir($kernel->getContainer()->getParameter('kernel.cache_dir'));

require_once 'http.php';
require_once 'websocket.php';

Worker::runAll();
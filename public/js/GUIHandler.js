class GUIHandler {

    /**
     * @returns {jQuery.fn.init|jQuery|HTMLElement}
     */
    getPreviousDirectoryLineItem = (previousDir) => {
        //fix: create dir -> go into -> try to go back -> id is undefined
        let row = $(`<tr id="directory-content-prev-level"><td class="py-1">..</td></tr>`);
        row.on('dblclick', function () {
            gui.changeDirectoryTo(previousDir);
        });

        return row;
    };

    /**
     * @param item {Object}
     * @returns {jQuery|HTMLElement}
     */
    getLineItem = (item) => {
        let row = $(`<tr class="directory-content-row" id="${item.isDir ? 'd' : 'f'}_${item.id}" ${item.isDir ? 'style="background-color: #AAAAAA;"' : ''}></tr>`);
        let nameCell = $(`<td class="name col-6 py-1"><span>${item.name}</span></td>`);
        let infoBtn = $('<button class="unit-info">info</button>');
        let deleteBtn = $('<button class="delete-btn">delete</button>');

        infoBtn.on('click', item, function (event) {
            const item = event.data;
            console.log('info of '+item.name+' '+(item.isDir ? 'd' : 'f')+'_'+item.id);
            if(item.isDir){
                dirHandler.directoryInfo(item.id, null);
            } else {
                dirHandler.fileInfo(item.id, null);
            }
        });
        deleteBtn.on('click', item, function (event) {
            const item = event.data;
            console.log('removing '+item.name+' '+(item.isDir ? 'd' : 'f')+'_'+item.id);
            if(item.isDir){
                dirHandler.deleteDirectory(item.id);
            } else {
                dirHandler.deleteFile(item.id);
            }
        });
        nameCell.append(infoBtn, deleteBtn);

        let fileSizeCell = $(`<td class="file-size col-2">${item.fileSize ?? item.directorySize}</td>`);
        let lastAccessCell = $(`<td class="last-access col-2">${item.lastAccess}</td>`);
        let lastModifiedCell = $(`<td class="last-modified col-2">${item.lastModified}</td>`);

        row.append(nameCell, fileSizeCell, lastAccessCell, lastModifiedCell);
        if (item.isDir === true) {
            row.on('dblclick', function () {
                gui.changeDirectoryTo(item);
            });
        }

        return row;
    };

    changeDirectoryTo = (directory) => {
        currentDirectory.id = directory.id;
        currentDirectory.name = directory.name;
        currentDirectory.parentDir = directory.parentDir;
        $('span#cur_directory').text(directory.name);
        dirHandler.updateDirectoryContent(directory.id);
    };

    /**
     * @returns {jQuery.fn.init|jQuery|HTMLElement}
     */
    getDirectoryContentTable = () => {
        return $('table#directory-content-table');
    };

    getItemFormContainer = (id, form) => {
        let container = $("<div id='form-container' class='d-flex flex-column'></div>")
            .css({
                "position": "absolute",
                "border": "1px solid black",
                "top": "250px",
                "left": "250px",
                "background-color": "white"
            });
        let closeBtn = $("<button id='close-btn'>X</button>");
        closeBtn.on('click', function () {
            $('div#form-container').remove();
        });
        let submitBtn = $("<button id='submit-btn' class='btn btn-primary'>submit</button>");
        submitBtn.on('click', function () {
            let formData = new FormData($('form[name=directory_unit]')[0]);
            let returnArray = {};
            for (let [key, value] of formData.entries()) {
                returnArray[key] = value;
            }
            console.log(returnArray);
            client.sendRequest('app_unit_info', {
                'unitForm': returnArray,
                'unit': id
            }, dirHandler.directoryUnitFormSubmitCallback);

        });
        let toolbar = $("<div id='form-toolbar' class='d-flex justify-content-between'><span>form name</span></div>");
        toolbar.append(submitBtn);
        toolbar.append(closeBtn);
        let contentArea = $("<div id='form-content' class='d-flex'></div>");
        contentArea.append(form);
        container.append(toolbar, contentArea);

        return container;
    };

    /**
     * @param lineItem {jQuery|HTMLElement}
     * @param unit {Object}
     */
    updateLineItem(lineItem, unit) {
        lineItem.find('td.name > span').text(unit.name);
        lineItem.find('td.last-access').text(unit.lastAccess);
        lineItem.find('td.last-modified').text(unit.lastModified);
    }

    /**
     * @param item {Object}
     */
    getDirectoryUnitForm(item) {
        let form = $(`<form id="directory_unit_info_${item.id}" name="directory_unit"></form>`);
        let label = $(`<label>Name:</label>`);
        let input = $(`<input type="text" name="name">`);
        input.val(item.name);
        label.append(input);
        form.append(label);

        return form;
    }

    getFormErrorNotif(errorMessage) {
        return $(`<small style="color: red;">${errorMessage}</small>`)
    }
}
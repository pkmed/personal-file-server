let client, gui, dirHandler;
let currentDirectory = {};

$.ajax({
    url: location.origin + '/app/token',
    success: start,
});

function start(token) {
    client = new WebsocketClient('localhost', '2346', token);
    gui = new GUIHandler();
    dirHandler = new DirectoryContentHandler();

    if (localStorage.getItem('curDir') === null) {
        localStorage.setItem('curDir', JSON.stringify(defaultCurrentDirectory));
        currentDirectory = defaultCurrentDirectory;
    } else {
        currentDirectory = JSON.parse(localStorage.getItem('curDir'));
    }
}

function init() {
    $('span#cur_directory').text(currentDirectory.name);
    $('button#create_dir_btn').on('click', () => {
        let dirName = prompt('Enter a name of the directory.');
        if (dirName !== "" || dirName.length !== 0) {
            dirHandler.createDirectory(dirName, currentDirectory.id);
        }
    });
    $("input#upload_file").on('input', function () {
        let file = this.files[0];
        let reader = new FileReader();
        reader.onload = function (e) {
            /** @var {ArrayBuffer} rawData */
            let rawData = e.target.result;
            const string = new Uint8Array(rawData).reduce((data, byte) => data + String.fromCharCode(byte), '');
            let base64String = btoa(string);
            console.log(file);
            let fileToUpload = {
                'name': file.name,
                'mime': file.type,
                'size': file.size,
                'content': base64String
            };
            console.log(fileToUpload);
            dirHandler.uploadFile(fileToUpload, currentDirectory.id);
        };
        reader.readAsArrayBuffer(file);
    });
    dirHandler.updateDirectoryContent(currentDirectory.id);
}
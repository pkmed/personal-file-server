class WebsocketClient {
    requestQueueId = 0;
    requestQueue = {};
    token = '';
    serverCalls = {};

    /**
     * @param wsUrl {String}
     * @param wsPort {String}
     * @param token {String} JWT
     */
    constructor(wsUrl, wsPort, token) {
        this.token = token;
        this.socket = new WebSocket('ws://' + wsUrl + ':' + wsPort);
        this.socket.onmessage = this.onMessage;
        this.socket.onclose = this.onClose;
        this.socket.onerror = this.onError;
        this.socket.onopen = this.onOpen;
        this.serverCalls = {
            'ut': this.updateToken,
            'em': this.displayErrorMessage
        };
    };

    /**
     * @param action {String}
     * @param data {object}
     * @param callback {function}
     */
    sendRequest = (action, data, callback) => {
        let requestId = 'i_' + ++this.requestQueueId;
        this.requestQueue[requestId] = callback;

        const requestObj = {
            'reqId': requestId,
            'token': this.token,
            'action': action,
            'data': data
        };
        this.socket.send(JSON.stringify(requestObj));
    };

    onMessage = (event) => {
        let data;
        try {
            data = JSON.parse(event.data);
        } catch (er) {
            console.log('socket parse error: ' + event.data);
        }

        let requestId = data.reqId;
        if (this.serverCalls.hasOwnProperty(requestId)) {
            this.serverCalls[requestId](data);
        } else if (typeof requestId != 'undefined' && typeof (this.requestQueue[requestId]) == 'function') {
            let responseCallback = this.requestQueue[requestId];
            responseCallback(...Object.values(data.data));
            delete this.requestQueue[requestId];
        } else {
            this.socketReceiveData(event.data);
        }
    };

    updateToken = (data) => {
        this.token = data.token;
    };

    displayErrorMessage = (response) => {
        console.log(`[error] Code=${response.data.code} reason='${response.data.message}'`);
    };

    socketReceiveData = (data) => {
        console.log(data);
    };

    onClose = (event) => {
        if (event.wasClean) {
            console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
        } else {
            console.log(event);
        }
    };

    onError = (error) => {
        console.log(`[error] ${error.message}`);
    };

    getState = () => {
        return this.socket.readyState;
    };

    onOpen = () => {
        init();
    };
}
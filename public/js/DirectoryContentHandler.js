class DirectoryContentHandler {
    contentList = gui.getDirectoryContentTable();

    /**
     * @param directoryId {number}
     */
    updateDirectoryContent = (directoryId) => {
        client.sendRequest('csb_directory_content', {'dir': directoryId}, this.updateDirectoryContentCallback);
    };

    updateDirectoryContentCallback = (directories, files) => {
        console.log(directories);
        console.log(files);
        this.contentList.empty();
        if (currentDirectory.id !== 1) {
            this.contentList.append(gui.getPreviousDirectoryLineItem(currentDirectory.parentDir));
        }
        directories.forEach((item) => {
                item.isDir = true;
                let lineItem = gui.getLineItem(item);
                this.contentList.append(lineItem);
            }
        );
        files.forEach((item) => {
                let lineItem = gui.getLineItem(item);
                this.contentList.append(lineItem);
            }
        );
    };

    createDirectory = (dirName, parentDir) => {
        client.sendRequest('csb_create_directory', {
            'dirName': dirName,
            'parentDir': parentDir
        }, this.createDirectoryCallback);
    };

    createDirectoryCallback = (directory) => {
        console.log(directory);
        directory.isDir = true;
        let lineItem = gui.getLineItem(directory);
        this.contentList.append(lineItem);
    };

    uploadFile = (file, parentDir) => {
        client.sendRequest('csb_upload_file', {
            'file': file,
            'parentDir': parentDir
        }, this.uploadFileCallback);
    };

    uploadFileCallback = (file) => {
        console.log(file);
        let lineItem = gui.getLineItem(file);
        this.contentList.append(lineItem);
    };

    deleteFile = (fileId) => {
        client.sendRequest('csb_delete_file', {
            'file': fileId.replace('f_', '')
        }, this.deleteFileCallback);
    };

    deleteFileCallback = (result, fileId) => {
        if (result) {
            this.contentList.children('tr#f_' + fileId).remove();
        }
    };

    deleteDirectory = (dirId) => {
        client.sendRequest('csb_delete_directory', {
            'dir': dirId
        }, this.deleteDirectoryCallback);
    };

    deleteDirectoryCallback = (result, dirId) => {
        if (result) {
            this.contentList.children('tr#d_' + dirId).remove();
        }
    };

    // unitInfo = (id, form = null) => {
    //     client.sendRequest('app_unit_info', {
    //         'unitForm': form,
    //         'unit': id
    //     }, this.unitInfoCallback);
    // };
    //
    // unitInfoCallback = (unit) => {
    //     let formContainer = gui.getItemFormContainer(unit.id, gui.getDirectoryUnitForm(unit));
    //     $('div#work-area').append(formContainer);
    // };
    //
    // directoryUnitFormSubmitCallback = (data) => {
    //     if (data.hasOwnProperty('id')) {
    //         this.updateDirectoryUnit(data);
    //     } else {
    //         this.displayFormErrors(data);
    //     }
    // };
    //
    // updateDirectoryUnit = (unit) => {
    //     if (unit.parentDir.id !== currentDirectory.id) {
    //         $(`tr.directory-content-row#${unit.id}`).remove();
    //     } else {
    //         gui.updateLineItem($(`tr.directory-content-row#${unit.id}`), unit);
    //     }
    //     $('div#form-container').remove();
    // };
    //
    // displayFormErrors = (errors) => {
    //     console.log(errors);
    //     Object.entries(errors).forEach(([fieldName, field]) => {
    //         let input = $(`div#form-container input[name="${fieldName}"]`);
    //         let errorNotif = gui.getFormErrorNotif(field[0]);
    //         errorNotif.insertBefore(input);
    //     });
    // };
}
<?php

use App\PFS\CloudStorageBundle\Constants\ServerCalls;
use App\PFS\CloudStorageBundle\Security\WSTokenHandler;
use Workerman\Connection\TcpConnection;
use Workerman\Timer;
use Workerman\Worker;

//time in seconds
define('WS_TOKEN_TTL', 900);

$ws_worker       = new Worker($_ENV['WEBSOCKET_URL']);
$ws_worker->name = 'psf_ws';

// 1 process
$ws_worker->count = 1;

$ws_worker->onWorkerStart = static function (Worker $worker) {
    Timer::add(
        WS_TOKEN_TTL,
        static function ($worker) {
            $token = WSTokenHandler::generateToken();
            foreach ($worker->connections as $connection) {
                $connection->send(json_encode(['reqId' => ServerCalls::UPDATE_TOKEN, 'token' => $token]));
            }
        },
        [$worker]
    );
};

$ws_worker->onMessage = static function (TcpConnection $connection, $data) use (&$kernel) {
    $connection->send($kernel->handleWS($data));
};
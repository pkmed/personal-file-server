<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210112160914 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE directory_unit (id INT AUTO_INCREMENT NOT NULL, parent_dir_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, mime_type VARCHAR(255) NOT NULL, file_path VARCHAR(255) NOT NULL, extension VARCHAR(255) NOT NULL, file_size DOUBLE PRECISION NOT NULL, trash TINYINT(1) NOT NULL, last_modified DATE NOT NULL, last_access DATE NOT NULL, is_dir TINYINT(1) NOT NULL, INDEX IDX_AA6C05B1A1A04E20 (parent_dir_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE directory_unit ADD CONSTRAINT FK_AA6C05B1A1A04E20 FOREIGN KEY (parent_dir_id) REFERENCES directory_unit (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directory_unit DROP FOREIGN KEY FK_AA6C05B1A1A04E20');
        $this->addSql('DROP TABLE directory_unit');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210626182500 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directory_unit DROP FOREIGN KEY FK_AA6C05B1A1A04E20');
        $this->addSql('CREATE TABLE directory (id INT AUTO_INCREMENT NOT NULL, parent_dir_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, file_path VARCHAR(255) NOT NULL, trash TINYINT(1) NOT NULL, last_modified DATE NOT NULL, last_access DATE NOT NULL, INDEX IDX_467844DAA1A04E20 (parent_dir_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file (id INT AUTO_INCREMENT NOT NULL, parent_dir_id INT DEFAULT NULL, mime_type VARCHAR(255) NOT NULL, extension VARCHAR(255) NOT NULL, file_size DOUBLE PRECISION NOT NULL, name VARCHAR(255) NOT NULL, file_path VARCHAR(255) NOT NULL, trash TINYINT(1) NOT NULL, last_modified DATE NOT NULL, last_access DATE NOT NULL, INDEX IDX_8C9F3610A1A04E20 (parent_dir_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE directory ADD CONSTRAINT FK_467844DAA1A04E20 FOREIGN KEY (parent_dir_id) REFERENCES directory (id)');
        $this->addSql('ALTER TABLE file ADD CONSTRAINT FK_8C9F3610A1A04E20 FOREIGN KEY (parent_dir_id) REFERENCES directory (id)');
        $this->addSql('DROP TABLE directory_unit');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE directory DROP FOREIGN KEY FK_467844DAA1A04E20');
        $this->addSql('ALTER TABLE file DROP FOREIGN KEY FK_8C9F3610A1A04E20');
        $this->addSql('CREATE TABLE directory_unit (id INT AUTO_INCREMENT NOT NULL, parent_dir_id INT DEFAULT NULL, name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, mime_type VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, file_path VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, extension VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, file_size DOUBLE PRECISION NOT NULL, trash TINYINT(1) NOT NULL, last_modified DATE NOT NULL, last_access DATE NOT NULL, is_dir TINYINT(1) NOT NULL, INDEX IDX_AA6C05B1A1A04E20 (parent_dir_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE directory_unit ADD CONSTRAINT FK_AA6C05B1A1A04E20 FOREIGN KEY (parent_dir_id) REFERENCES directory_unit (id)');
        $this->addSql('DROP TABLE directory');
        $this->addSql('DROP TABLE file');
    }
}

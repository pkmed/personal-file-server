<?php

namespace App\PFS\CoreBundle;

use App\PFS\CloudStorageBundle\Constants\ServerCalls;
use App\PFS\CloudStorageBundle\Security\WSTokenHandler;
use Exception;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadataFactory;
use Symfony\Component\Routing\RouteCollection;

class WebSocketKernel
{
    /** @var ArgumentMetadataFactory */
    private $argumentMetadataFactory;

    /** @var RouteCollection $webSocketRoutes */
    private $webSocketRoutes;

    /** @var ContainerInterface $container */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->argumentMetadataFactory = new ArgumentMetadataFactory();
        $this->webSocketRoutes         = new RouteCollection();
        $this->container               = $container;

        $this->webSocketRoutes->addCollection(
            $container->get('routing.loader')->load('@CloudStorageBundle/Resources/config/websocket_routing.yaml')
        );
    }

    /**
     * @param string $data Json data
     * @return false|string
     */
    public function handle(string &$data)
    {
        self::SafeEcho('====================================='.PHP_EOL);
        self::MemoryConsumption('Before processing request');
        $data = json_decode($data, true);
        try {
            return $this->handleWS($data);
        } catch (Exception $e) {
            return $this->returnInternalError($e);
        }
    }

    /**
     * @param array $data
     * @return false|string
     */
    private function handleWS(array &$data)
    {
        if (!WSTokenHandler::checkToken($data['token'])) {
            $resp = ['reqId' => ServerCalls::ERROR_MESSAGE, 'data' => ['Unauthenticated access.']];

            return json_encode($resp);
        }

        // 1. define which method need to be called
        $route = $this->webSocketRoutes->get($data['action']);
        if ($route === null) {
            throw new RuntimeException('Route was not found', 400);
        }

        $callable   = explode('::', $route->getDefault('_controller'));
        $controller = $this->container->get($callable[0]);
        if ($controller === null) {
            throw new RuntimeException('Controller was not found', 500);
        }

        $arguments = $this->getArguments($data, $callable);
        $respData  = $controller->{$callable[1]}(...$arguments);
        $resp      = ['reqId' => $data['reqId'], 'data' => $respData];

        unset($route, $callable, $controller, $arguments, $respData);

        gc_collect_cycles();
        self::MemoryConsumption('After processing request');
        self::SafeEcho('====================================='.PHP_EOL);

        return json_encode($resp);
    }

    private function getArguments(array &$data, array &$callable): array
    {
        //define list of arguments of this method
        $argumentMetadata = $this->argumentMetadataFactory->createArgumentMetadata($callable);
        //assemble arguments in proper order in array
        $arguments = [];
        foreach ($argumentMetadata as &$argumentMetadatum) {
            if (preg_match("/^([A-Z][a-z0-9_]+(\\\)?)+/", $argumentMetadatum->getType()) === 1) {
                //case if argument is an entity:
                //if argument's name is the same as a field in $data, then it is an id
                //fetch service otherwise
                if (isset($data['data'][$argumentMetadatum->getName()])) {
                    $arguments[] = $this->container->get('doctrine.orm.entity_manager')->find(
                        $argumentMetadatum->getType(),
                        $data['data'][$argumentMetadatum->getName()]
                    );
                } else {
                    $arguments[] = $this->container->get($argumentMetadatum->getType());
                    //get all necessary dependencies from list
                }
            } else {
                //get all arguments from a request
                $arguments[] = $data['data'][$argumentMetadatum->getName()];
            }
        }
        unset($argumentMetadata, $argumentMetadatum);

        return $arguments;
    }

    private function returnInternalError(Exception $exception)
    {
        return json_encode(
            [
                'reqId' => ServerCalls::ERROR_MESSAGE,
                'data'  => ['message' => $exception->getMessage(), 'code' => $exception->getCode()],
            ]
        );
    }

    public static function MemoryConsumption(string $describer): void
    {
        self::SafeEcho($describer." memory consumption: ".round(((memory_get_usage() / 1024) / 1024), 3)."MB".PHP_EOL);
    }

    public static function SafeEcho(string $msg): void
    {
        fwrite(STDOUT, $msg);
    }
}
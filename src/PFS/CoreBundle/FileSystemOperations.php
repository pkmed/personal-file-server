<?php


namespace App\PFS\CoreBundle;

/**
 * Class FileSystemOperations
 * Contains operations with files and directories on a file system level
 *
 * @package App\PFS\CoreBundle
 */
class FileSystemOperations
{
    /**
     * @param string $newDirName    Name of a new directory
     * @param string $parentDirPath Parent dir of the directory
     * @return bool                 True, if directory was created successfully
     */
    public static function createDirectory(string $newDirName, string $parentDirPath): bool
    {
        $newDirPath = $parentDirPath.'/'.$newDirName;

        return mkdir($newDirPath, 0760);
    }

    /**
     * Creates a new file with a content
     *
     * @param string $content  File content
     * @param string $filePath Full, absolute file path
     * @return bool TRUE if file was created
     */
    public static function createNewFile(string $content, string $filePath): bool
    {
        $ifp = fopen($filePath, 'wb');
        fwrite($ifp, $content);
        fclose($ifp);

        return file_exists($filePath);
    }

    /**
     * Adds an order number to the item's name if there is items with the same name
     *
     * @param string $itemName
     * @param string $parentDirPath
     * @return string Changed item name
     */
    public static function handleDuplicateDirectoryItemName(string $itemName, string $parentDirPath): string
    {
        $pathinfo              = pathinfo($parentDirPath.'/'.$itemName);
        $pathinfo['extension'] = $pathinfo['extension'] ?? '';
        if (!empty(
        preg_grep(
            "~{$pathinfo['filename']}(\(\d+\))?\.?{$pathinfo['extension']}?~",
            scandir($parentDirPath)
        )
        )) {
            $itemName = $pathinfo['filename'].'('.self::getDuplicateAmountOfItem($itemName, $parentDirPath).')'
                .(!empty($pathinfo['extension']) ? '.'.$pathinfo['extension'] : '');
        }
        unset($pathinfo, $parentDirPath);

        return $itemName;
    }

    /**
     * @param string $itemName      Name of the item, which suppose to be created
     * @param string $parentDirPath Parent dir of the item
     * @return int                  A count of the item's duplicates
     */
    public static function getDuplicateAmountOfItem(string $itemName, string $parentDirPath): int
    {
        $pathinfo              = pathinfo($parentDirPath.'/'.$itemName);
        $pathinfo['extension'] = $pathinfo['extension'] ?? '';

        $count = count(
            preg_grep("~{$pathinfo['filename']}(\(\d+\))?\.?{$pathinfo['extension']}?~", scandir($parentDirPath))
        );
        unset($itemName, $parentDirPath, $pathinfo);

        return $count;
    }

    /**
     * @param string $filePath
     * @return mixed
     */
    public static function getFileExtension(string $filePath)
    {
        return pathinfo($filePath)['extension'] ?? '';
    }

    /**
     * @param string $filePath
     * @return mixed
     */
    public static function getFileMimeType(string $filePath)
    {
        return finfo_file(finfo_open(FILEINFO_MIME_TYPE), $filePath);
    }

    /**
     * @param string $filePath Full path to the file
     * @return bool True on success
     */
    public static function deleteFile(string $filePath): bool
    {
        if (file_exists($filePath)) {
            return unlink($filePath);
        }

        return true;
    }

    /**
     * @param string $dirPath Full path to the directory
     * @return bool True on success
     */
    public static function deleteDirectory(string $dirPath)
    {
        $dirs = glob("$dirPath/*", GLOB_ONLYDIR);
        foreach ($dirs as $dir) {
            self::deleteDirectory($dir);
        }

        $files = glob("$dirPath/*", GLOB_NOSORT);
        foreach ($files as $file) {
            self::deleteFile($file);
        }

        return rmdir($dirPath);
    }
}
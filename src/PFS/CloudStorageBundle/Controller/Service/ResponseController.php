<?php

namespace App\PFS\CloudStorageBundle\Controller\Service;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ResponseController extends AbstractController
{
    private $serializer;

    public function __construct()
    {
        $this->serializer = new Serializer(
            [
                new DateTimeNormalizer(['datetime_format' => 'd-m-Y H:i:s']),
                new ObjectNormalizer(
                    new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader())),
                    null,
                    null,
                    null,
                    null,
                    null,
                    $defaultContext = [
                        AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => static function (object $object) {
                            return $object->getName();
                        },
                    ]
                ),
            ], [new JsonEncoder()]
        );
    }

    /**
     * @param mixed $data Object to send
     * @param array|string $serializationGroups Serialization groups for a normalization
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function createNormalizedResponse($data, $serializationGroups): array
    {
        return $this->serializer->normalize(
            $data,
            'json',
            ['groups' => $serializationGroups]
        );
    }
}

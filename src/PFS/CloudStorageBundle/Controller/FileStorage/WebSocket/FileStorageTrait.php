<?php


namespace App\PFS\CloudStorageBundle\Controller\FileStorage\WebSocket;


use App\PFS\CloudStorageBundle\Entity\FileStorage\Directory;
use App\PFS\CloudStorageBundle\Entity\FileStorage\DirectoryUnit;

trait FileStorageTrait
{
    abstract public function create(string $name, Directory $parentDir);

    abstract public function delete(DirectoryUnit $unit);

    abstract public function upload(array $file, Directory $parentDir);

    abstract public function download(DirectoryUnit $unit);

    abstract public function editProperties(?array $unitForm, DirectoryUnit $unit);
}
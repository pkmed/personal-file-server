<?php


namespace App\PFS\CloudStorageBundle\Controller\FileStorage\WebSocket;


use App\PFS\CloudStorageBundle\Entity\FileStorage\Directory;
use App\PFS\CloudStorageBundle\Entity\FileStorage\File;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class FileController
 *
 * @package App\PFS\CloudStorageBundle\Controller\FileStorage\WebSocket
 */
class FileController extends AbstractFileStorageController
{
    use FileStorageTrait;

    /**
     * FileController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @param string $name
     * @param Directory $parentDir
     */
    public function create(string $name, Directory $parentDir)
    {
    }

    /**
     * @Route(name="csb_delete_file", schemes={"ws", "wss"})
     * @param File $file
     * @return array
     * @throws ORMException
     */
    public function delete(File $file): array
    {
        return $this->fsHandler->deleteFile($file);
    }

    /**
     * @Route(name="csb_upload_file", schemes={"ws", "wss"})
     * @param array $file
     * @param Directory $parentDir
     * @return array
     * @throws ExceptionInterface
     */
    public function upload(array $file, Directory $parentDir): array
    {
        $newFileEntity = $this->fsHandler->createFile($file, $parentDir);

        return $this->createNormalizedResponse($newFileEntity, ['get_dir_content']);
    }

    public function download(File $file)
    {
        //TODO: implement file downloading
    }

    public function editProperties(?array $unitForm, File $file)
    {
    }
}
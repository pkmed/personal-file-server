<?php


namespace App\PFS\CloudStorageBundle\Controller\FileStorage\WebSocket;


use App\PFS\CloudStorageBundle\Controller\Service\ResponseController;
use App\PFS\CloudStorageBundle\Service\FileStorage\FileStorageHandler;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class AbstractFileStorageController
 *
 * @package App\PFS\CloudStorageBundle\Controller\FileStorage
 */
abstract class AbstractFileStorageController extends ResponseController
{
    /** @var EntityManagerInterface */
    protected $em;
    /** @var FileStorageHandler */
    protected $fsHandler;

    protected function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em        = $em;
        $this->fsHandler = new FileStorageHandler($em);
    }

    /**
     * Formatting keys in nested arrays
     *
     * From:
     * <code>$array['key_A[key_subA]'] = 'value';</code>
     *
     * To:
     * <code>$array['key_A']['key_subA'] = 'value';</code>
     *
     * @param array $sentForm
     *
     * @return array
     */
    protected function formatNestedKeys(array $sentForm): array
    {
        $procForm = [];
        foreach ($sentForm as $key => $item) {
            if (preg_match("/\w+\[\w+]/", $key) !== 0) {
                [$entity, $field] = explode('[', $key);
                $field = str_replace(']', '', $field);
                if (!array_key_exists($entity, $procForm)) {
                    $procForm[$entity] = [
                        $field => $item,
                    ];
                } else {
                    $procForm[$entity][$field] = $item;
                }
            } else {
                $procForm[$key] = $item;
            }
        }

        return $procForm;
    }
}
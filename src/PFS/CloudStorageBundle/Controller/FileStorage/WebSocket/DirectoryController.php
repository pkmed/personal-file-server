<?php


namespace App\PFS\CloudStorageBundle\Controller\FileStorage\WebSocket;


use App\PFS\CloudStorageBundle\Entity\FileStorage\Directory;
use App\PFS\CloudStorageBundle\Entity\FileStorage\DirectoryUnit;
use App\PFS\CloudStorageBundle\Form\DirectoryUnitForm;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use RuntimeException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Class DirectoryController
 *
 * @package App\PFS\CloudStorageBundle\Controller\FileStorage
 */
class DirectoryController extends AbstractFileStorageController
{
    use FileStorageTrait;

    /**
     * DirectoryController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @Route(name="csb_directory_content", schemes={"ws", "wss"})
     * @param Directory $dir
     *
     * @return DirectoryUnit[]
     * @throws ExceptionInterface
     */
    public function getDirectoryContent(Directory $dir): array
    {
        if ($dir !== null) {
            return $this->createNormalizedResponse(
                ['directories' => $dir->getChildDirectories(), 'files' => $dir->getChildFiles()],
                ['get_dir_content', 'get_parent_dir']
            );
        }

        throw new RuntimeException("Directory was not found");
    }

    /**
     * @Route(name="csb_create_directory", schemes={"ws", "wss"})
     * @param string $dirName
     * @param Directory $parentDir
     * @return array
     * @throws ExceptionInterface
     */
    public function create(string $dirName, Directory $parentDir): array
    {
        if ($parentDir !== null) {
            $newDir = $this->fsHandler->createDirectory(
                $dirName,
                $parentDir
            );

            return $this->createNormalizedResponse(['directory' => $newDir], ['get_dir_content']);
        }

        throw new RuntimeException("Parent directory doesn't exists");
    }

    /**
     * @Route(name="csb_delete_directory", schemes={"ws", "wss"})
     * @param Directory $dir
     * @return mixed|void
     * @throws ORMException
     */
    public function delete(Directory $dir)
    {
        return $this->fsHandler->deleteDirectory($dir);
    }

    /**
     * Upload directory
     *
     * @param array $directory
     * @param Directory $parentDir
     */
    public function upload(array $directory, Directory $parentDir)
    {
    }

    public function download(Directory $directory)
    {
    }

    /**
     * @Route(name="csb_directory_info", schemes={"ws", "wss"})
     *
     * @param array|null $unitForm
     * @param Directory $directory
     * @return array
     * @throws ExceptionInterface
     */
    public function editProperties(?array $unitForm, Directory $directory): array
    {
        //TODO: think about how to build a form processing flow

        if ($unitForm !== null && $directory !== null) {
            //todo: create a separate form classes for the File and Directory entities
            $formClass = new DirectoryUnitForm($directory);

            $this->formatNestedKeys($unitForm);

            $validationResult = $formClass->validateForm($unitForm);
            if ($validationResult === true) {
                $formClass->updateEntity($unitForm);
                $date = new DateTime();
                $directory->setLastAccess($date);
                $directory->setLastModified($date);

                $this->em->persist($directory);
                $this->em->flush();
            }
            unset($unitForm, $formClass, $date, $directory);

            return $this->createNormalizedResponse($validationResult, []);
        }

        return $this->createNormalizedResponse($directory, ['get_dir_content', 'get_parent_dir']);
    }
}
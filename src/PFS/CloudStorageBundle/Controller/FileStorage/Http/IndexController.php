<?php

namespace App\PFS\CloudStorageBundle\Controller\FileStorage\Http;

use App\PFS\CloudStorageBundle\Controller\Service\ResponseController;
use App\PFS\CloudStorageBundle\Repository\FileStorage\DirectoryRepository;
use App\PFS\CloudStorageBundle\Security\WSTokenHandler;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;

/**
 * Class IndexController
 *
 * @Route("/app", schemes={"http", "https"})
 *
 * @package App\PFS\CloudStorageBundle\Controller\FileStorage\Http
 */
class IndexController extends ResponseController
{
    /** @var DirectoryRepository */
    private $directoryRepository;

    /**
     * IndexController constructor.
     *
     * @param DirectoryRepository $directoryRepository
     */
    public function __construct(DirectoryRepository $directoryRepository)
    {
        parent::__construct();
        $this->directoryRepository = $directoryRepository;
    }

    /**
     * @Route("/", name="csb_index")
     *
     * Displays main page of the app or redirects to login if the user is not authenticated
     * @IsGranted("ROLE_USER")
     *
     * @return Response
     */
    public function index(): Response
    {
        if ($this->get('security.token_storage')->getToken() instanceof AnonymousToken) {
            return $this->redirectToRoute('app_login');
        }

        return $this->render(
            'app/index.html.twig',
            [
                'rootDir' => $this->directoryRepository->findOneBy([], ['id' => 'ASC']),
            ]
        );
    }

    /**
     * @Route("/token", name="csb_get_token")
     *
     * Emits the token for the websocket requests after login
     * @IsGranted("ROLE_USER")
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getToken(): JsonResponse
    {
        return new JsonResponse(WSTokenHandler::generateToken());
    }
}

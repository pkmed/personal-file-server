<?php


namespace App\PFS\CloudStorageBundle\Form;

use App\PFS\CloudStorageBundle\Components\Form\AbstractForm;
use Symfony\Component\Validator\Constraints\NotBlank;

class DirectoryUnitForm extends AbstractForm
{
    /**
     * {@inheritDoc}
     */
    public function configureForm(): void
    {
        $this->addField(
            'name',
            [
                new NotBlank(['message' => 'Name should not be blank']),
            ]
        );
    }
}
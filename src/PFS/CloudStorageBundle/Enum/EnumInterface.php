<?php


namespace App\PFS\CloudStorageBundle\Enum;

/**
 * Interface EnumInterface
 * @package App\Enum
 */
interface EnumInterface
{
    /**
     * @return array
     */
    public static function getAllElements(): array;

    /**
     * @param int $id Id of an element
     */
    public static function getElementById(int $id);

    /**
     * Returns an id of element with a corresponding title
     *
     * @param string $title Title of an element
     */
    public static function getElementIdByTitle(string $title);
}
<?php


namespace App\PFS\CloudStorageBundle\Enum;

/**
 * Class FileExtensions
 *
 * @package App\Enum
 */
class FileExtensions implements EnumInterface
{
    /**
     * Supported extensions
     */
    private const EXTENSIONS = [
        //text
        'txt',
        //image
        'jpg',
        'png',
        'svg',
        //video
        'mp4',
        'webm',
        //audio
        'mp3',
        'm4a',
        'flac',
        'wav',
        'wma',
    ];

    /**
     * {@inheritDoc}
     *
     * @return array Returns all supported extensions
     */
    public static function getAllElements(): array
    {
        return self::EXTENSIONS;
    }

    /**
     * {@inheritDoc}
     *
     * @return string Name of a supported extension
     */
    public static function getElementById(int $id): string
    {
        return self::EXTENSIONS[$id];
    }

    /**
     * {@inheritDoc}
     *
     * @return int Id of a supported extension
     */
    public static function getElementIdByTitle(string $title): int
    {
        return array_search($title, self::EXTENSIONS, true);
    }
}
<?php

namespace App\PFS\CloudStorageBundle\Command;

use App\PFS\CloudStorageBundle\Entity\FileStorage\Directory;
use App\PFS\CloudStorageBundle\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Yaml\Yaml;

class InitCommand extends Command
{
    protected static $defaultName = 'app:init';
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var KernelInterface
     */
    private $kernel;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        EntityManagerInterface $em,
        KernelInterface $kernel,
        string $name = null
    ) {
        parent::__construct($name);
        $this->encoder = $encoder;
        $this->em      = $em;
        $this->kernel  = $kernel;
    }

    protected function configure()
    {
        $this
            ->setDescription(
                'Setup login and password for the account, create root directory depending on .env config'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io          = new SymfonyStyle($input, $output);
        $login       = $io->ask('Enter a login');
        $password    = $io->askHidden('Enter a password');
        $rootDirPath = $io->ask(
            'Enter an absolute path of the root dir',
            '',
            function ($answer) {
                return $this->validateRootDirectoryPath($answer);
            }
        );

        $user = new User();
        $user->setLogin($login);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setRoles(['ROLE_USER']);
        $user->setLastLogin((new DateTime()));

        $this->em->persist($user);

        $io->success('Account was successfully created!');

        if (is_dir($rootDirPath) && mkdir($rootDirPath, 660)) {
            $rootDir = new Directory();
            $rootDir->setFilePath($rootDirPath);
            $rootDir->setLastAccess(new DateTime());
            $rootDir->setLastModified(new DateTime());
            $rootDir->setParentDir(null);
            $rootDir->setTrash(false);
            $dirName = array_reverse(explode('/', $rootDirPath));
            if (empty($dirName[0])) {
                $dirName = $dirName[1];
            } else {
                $dirName = $dirName[0];
            }
            $rootDir->setName($dirName);

            $yaml                               = Yaml::parse(
                file_get_contents($this->kernel->getProjectDir().'/config/services.yaml')
            );
            $yaml['parameters']['psf_root_dir'] = $rootDirPath;
            file_put_contents($this->kernel->getProjectDir().'/config/services.yaml', Yaml::dump($yaml, 4));

            $this->em->persist($rootDir);

            $io->success('Root directory was successfully created!');
        } else {
            $io->error('Cannot create root directory at this path');

            return Command::FAILURE;
        }

        $this->em->flush();

        return Command::SUCCESS;
    }

    private function validateRootDirectoryPath(string $path): string
    {
        preg_replace('/\.{2}|[.~&%*!@#$^?]/', '', $path, -1, $replaces);

        if ($replaces > 0) {
            throw new RuntimeException("Path contains restricted characters ('..',.,~,&,%,*,!,@,#,$,^,?).");
        }

        return $path;
    }
}

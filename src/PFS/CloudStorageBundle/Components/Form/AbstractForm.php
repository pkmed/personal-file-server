<?php


namespace App\PFS\CloudStorageBundle\Components\Form;

use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Context\ExecutionContextFactory;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractForm
 *
 * @package App\Components\Form
 */
abstract class AbstractForm
{
    /**
     * @var RecursiveValidator
     */
    protected $validator;

    /**
     * @var object
     */
    protected $entity;

    /**
     * @var array
     */
    protected $fields = [];

    /**
     * AbstractForm constructor.
     *
     * @param object $entity
     * @param ValidatorInterface $validator
     */
    public function __construct(object $entity/*, ValidatorInterface $validator*/)
    {
        $contextFactory   = new ExecutionContextFactory(new Translator('en'));
        $metadataFactory  = new LazyLoadingMetadataFactory();
        $validatorFactory = new ConstraintValidatorFactory();
        $this->validator  = new RecursiveValidator($contextFactory, $metadataFactory, $validatorFactory);
        $this->entity     = $entity;
        $this->configureForm();
    }

    /**
     * @param $formData
     * @return array|bool Array of violations in case there are some. True on valid form.
     */
    public function validateForm($formData)
    {
        $violations = [];
        foreach ($this->fields as $fieldName => $constraints) {
            $violationsList = $this->validator->validate($formData[$fieldName], $constraints);
            if ($violationsList->count() > 0) {
                foreach ($violationsList as $violation) {
                    /** @var ConstraintViolationInterface $violation */
                    $violations[$fieldName][] = $violation->getMessage();
                }
            }
            unset($violationsList, $fieldName, $fieldValue, $constraints, $violation);
        }
        return count($violations) > 0 ? $violations : true;
    }

    /**
     * @param array $viewData
     */
    public function updateEntity(array $viewData): void
    {
        foreach ($viewData as $fieldName => $fieldValue) {
            if (is_string($fieldName)) {
                $setter = 'set'.ucfirst($fieldName);
                $this->entity->$setter($fieldValue);
            }
            unset($viewData, $fieldName, $fieldValue, $setter);
        }
    }

    /**
     * @param string $name
     * @param Constraint[] $constraints
     * @return AbstractForm
     */
    protected function addField(string $name, array $constraints): self
    {
        $this->fields[$name] = $constraints;

        return $this;
    }

    /**
     * Used for defining a list of the fields and constraints for ones by $this->addField()
     */
    abstract public function configureForm(): void;
}
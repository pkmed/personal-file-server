<?php

namespace App\PFS\CloudStorageBundle\Entity\FileStorage;

use App\PFS\CloudStorageBundle\Repository\FileStorage\FileRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=FileRepository::class)
 */
class File extends DirectoryUnit
{
    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="childFiles")
     */
    protected $parentDir;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get_dir_content"})
     */
    private $mimeType;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get_dir_content"})
     */
    private $extension;

    /**
     * @ORM\Column(type="float")
     * @Groups({"get_dir_content"})
     */
    private $fileSize;

    public function __construct(
        string $name = '',
        string $mimeType = '',
        int $fileSize = 0,
        Directory $parentDir = null,
        int $extension = 0,
        string $filePath = ''
    ) {
        parent::__construct($name, $parentDir, $filePath);
        $this->extension = $extension;
        $this->mimeType = $mimeType;
        $this->fileSize = $fileSize;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setMimeType(string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getFileSize(): ?float
    {
        return $this->fileSize;
    }

    public function setFileSize(float $fileSize): self
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    public function setParentDir(?Directory $parentDir): File
    {
        $this->parentDir = $parentDir;

        return $this;
    }
}

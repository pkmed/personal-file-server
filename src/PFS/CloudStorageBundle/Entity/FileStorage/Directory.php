<?php

namespace App\PFS\CloudStorageBundle\Entity\FileStorage;

use App\PFS\CloudStorageBundle\Repository\FileStorage\DirectoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=DirectoryRepository::class)
 * @HasLifecycleCallbacks
 */
class Directory extends DirectoryUnit
{
    /**
     * @var integer
     * @Groups({"get_dir_content"})
     */
    private $directorySize;

    /**
     * @ORM\OneToMany(targetEntity=Directory::class, mappedBy="parentDir", orphanRemoval=true)
     */
    private $childDirectories;

    /**
     * @ORM\OneToMany(targetEntity=File::class, mappedBy="parentDir", orphanRemoval=true)
     */
    private $childFiles;

    /**
     * @ORM\ManyToOne(targetEntity=Directory::class, inversedBy="childDirectories")
     */
    protected $parentDir;

    /**
     * Directory constructor.
     *
     * @param string $name
     * @param Directory|null $parentDir
     * @param string $filePath
     */
    public function __construct(
        string $name = '',
        Directory $parentDir = null,
        string $filePath = ''
    ) {
        parent::__construct($name, $parentDir, $filePath);
        $this->childDirectories = new ArrayCollection();
        $this->childFiles       = new ArrayCollection();
    }

    public function setParentDir(?Directory $parentDir): Directory
    {
        $this->parentDir = $parentDir;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getChildDirectories(): Collection
    {
        return $this->childDirectories;
    }

    public function addChildDirectory(Directory $childDirectory): self
    {
        if (!$this->childDirectories->contains($childDirectory)) {
            $this->childDirectories[] = $childDirectory;
            $childDirectory->setParentDir($this);
        }

        return $this;
    }

    public function removeChildDirectory(Directory $childDirectory): self
    {
        if ($this->childDirectories->removeElement($childDirectory)) {
            // set the owning side to null (unless already changed)
            if ($childDirectory->getParentDir() === $this) {
                $childDirectory->setParentDir(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|File[]
     */
    public function getChildFiles(): Collection
    {
        return $this->childFiles;
    }

    public function addChildFile(File $childFile): self
    {
        if (!$this->childFiles->contains($childFile)) {
            $this->childFiles[] = $childFile;
            $childFile->setParentDir($this);
        }

        return $this;
    }

    public function removeChildFile(File $childFile): self
    {
        if ($this->childFiles->removeElement($childFile)) {
            // set the owning side to null (unless already changed)
            if ($childFile->getParentDir() === $this) {
                $childFile->setParentDir(null);
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getDirectorySize(): int
    {
        return $this->directorySize ?? 0;
    }

    /**
     * Recalculate size of a directory on uploading, removing files, moving directories e.t.c.
     *
     * @return void
     */
    public function refreshDirectorySize(): void
    {
        foreach ($this->childDirectories as &$childDirectory) {
            /** @var Directory $childDirectory */
            $this->directorySize += $childDirectory->getDirectorySize();
        }
        unset($childDirectory);
        foreach ($this->childFiles as &$childFile) {
            /** @var File $childFile */
            $this->directorySize += $childFile->getFileSize();
        }
        unset($childFile);
    }
}

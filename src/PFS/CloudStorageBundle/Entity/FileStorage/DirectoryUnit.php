<?php

namespace App\PFS\CloudStorageBundle\Entity\FileStorage;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class DirectoryUnit
 *
 * @ORM\MappedSuperclass()
 * @package App\Entity\FileStorage
 */
abstract class DirectoryUnit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"get_dir_content"})
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get_dir_content"})
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get_dir_content"})
     */
    protected $filePath;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $trash;

    /**
     * @var DateTime
     * @ORM\Column(type="date")
     * @Groups({"get_dir_content"})
     */
    protected $lastModified;

    /**
     * @var DateTime
     * @ORM\Column(type="date")
     * @Groups({"get_dir_content"})
     */
    protected $lastAccess;

    /**
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"get_parent_dir"})
     */
    protected $parentDir;

    public function __construct(
        string $name = '',
        Directory $parentDir = null,
        string $filePath = ''
    )
    {
        $this->name = $name;
        $this->parentDir = $parentDir;
        $date = new DateTime();
        $this->lastModified = $date;
        $this->lastAccess = $date;
        $this->trash = false;
        $this->filePath = $filePath;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string Path to file or directory without trailing slash
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    public function getTrash(): ?bool
    {
        return $this->trash;
    }

    public function setTrash(bool $trash): self
    {
        $this->trash = $trash;

        return $this;
    }

    public function getLastModified(): ?DateTime
    {
        return $this->lastModified;
    }

    public function setLastModified(DateTime $lastModified): self
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    public function getLastAccess(): ?DateTime
    {
        return $this->lastAccess;
    }

    public function setLastAccess(DateTime $lastAccess): self
    {
        $this->lastAccess = $lastAccess;

        return $this;
    }

    public function getParentDir(): ?Directory
    {
        return $this->parentDir;
    }
}

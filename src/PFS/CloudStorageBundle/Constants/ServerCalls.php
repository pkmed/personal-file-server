<?php


namespace App\PFS\CloudStorageBundle\Constants;

/**
 * Class ServerCalls
 *
 * Contains request ids for a websocket server calls
 *
 * @package App\Constants
 */
final class ServerCalls
{
    /**
     * @var string Request id for a websocket token update
     */
    public const UPDATE_TOKEN = 'ut';

    /**
     * @var string Request id for a server error
     */
    public const ERROR_MESSAGE = 'em';
}
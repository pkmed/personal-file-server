<?php


namespace App\PFS\CloudStorageBundle\Service\FileStorage;


use App\PFS\CloudStorageBundle\Entity\FileStorage\Directory;
use App\PFS\CloudStorageBundle\Entity\FileStorage\File;
use App\PFS\CloudStorageBundle\Enum\FileExtensions;
use App\PFS\CoreBundle\FileSystemOperations;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use RuntimeException;

/**
 * Class FileStorageHandler
 * Links entities and files
 *
 * @package App\Service\FileStorage
 */
class FileStorageHandler
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * FileStorageHandler constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $name
     * @param Directory $parentDir
     *
     * @return Directory
     */
    public function createDirectory(string $name, Directory $parentDir): Directory
    {
        $name = FileSystemOperations::handleDuplicateDirectoryItemName($name, $parentDir->getFilePath());

        if (FileSystemOperations::createDirectory($name, $parentDir->getFilePath())) {
            $newDir = new Directory($name, $parentDir, $parentDir->getFilePath().'/'.$name);
            $this->em->persist($newDir);
            $this->em->flush();
            $parentDir->addChildDirectory($newDir);
            $this->em->flush();

            return $newDir;
        }

        throw new RuntimeException("Cannot create directory {$name} in {$parentDir->getFilePath()}");
    }

    /**
     * @param Directory $directory
     * @return array
     * @throws ORMException
     */
    public function deleteDirectory(Directory $directory): array
    {
        if (FileSystemOperations::deleteDirectory($directory->getFilePath())) {
            try {
                $id = $directory->getId();
                $this->em->remove($directory);
                $this->em->flush();

                return ['result' => true, 'dirId' => $id];
            } catch (ORMException $ormEx) {
                throw $ormEx;
            }
        }

        throw new RuntimeException("Cannot delete directory {$directory->getFilePath()}");
    }

    /**
     * @param array $file
     * @param Directory $parentDir
     *
     * @return File
     * @throws RuntimeException In case, when file cannot be created
     */
    public function createFile(array $file, Directory $parentDir): File
    {
        $file['name'] = FileSystemOperations::handleDuplicateDirectoryItemName(
            $file['name'],
            $parentDir->getFilePath()
        );
        $filePath     = $parentDir->getFilePath().'/'.$file['name'];

        if (FileSystemOperations::createNewFile(base64_decode($file['content']), $filePath)) {
            $extension = FileSystemOperations::getFileExtension($filePath);
            if (empty($file['mime'])) {
                $file['mime'] = FileSystemOperations::getFileMimeType($filePath);
            }

            $newFile = new File(
                $file['name'], $file['mime'], $file['size'],
                $parentDir, FileExtensions::getElementIdByTitle($extension), $filePath
            );
            $this->em->persist($newFile);
            $this->em->flush();
            $parentDir->addChildFile($newFile);
            $parentDir->refreshDirectorySize();
            $this->em->flush();

            return $newFile;
        }

        throw new RuntimeException("Cannot upload this file to {$parentDir->getFilePath()}");
    }

    /**
     * @param File $file
     * @return array
     * @throws ORMException
     * @throws RuntimeException
     */
    public function deleteFile(File $file): array
    {
        if (FileSystemOperations::deleteFile($file->getFilePath())) {
            try {
                $id = $file->getId();
                $this->em->remove($file);
                $this->em->flush();

                return ['result' => true, 'fileId' => $id];
            } catch (ORMException $ormEx) {
                throw $ormEx;
            }
        }

        throw new RuntimeException("Cannot delete file {$file->getFilePath()}");
    }

    //todo: implement methods below
//                $this->fsHandler->rename($unit, string $newName);
//                $this->fsHandler->moveTo($movingUnit, $newParentDir);
//                $this->fsHandler->changeExtension($unit, string $newExtension);
}
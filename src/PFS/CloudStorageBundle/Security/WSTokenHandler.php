<?php

namespace App\PFS\CloudStorageBundle\Security;

use App\PFS\CoreBundle\FileSystemOperations;
use Exception;

class WSTokenHandler
{
    private const TOKEN_FILE_NAME = '/token';
    private static $tokenFilePath;

    public static function setCacheDir(string $cacheDir): void
    {
        self::$tokenFilePath = $cacheDir.'/'.self::TOKEN_FILE_NAME;
    }

    /**
     * @return string Token
     * @throws Exception Thrown by random_bytes
     */
    public static function generateToken(): string
    {
        $token = bin2hex(random_bytes(32));
        FileSystemOperations::createNewFile($token, self::$tokenFilePath);

        return $token;
    }

    public static function checkToken(string $token): bool
    {
        if(file_exists(self::$tokenFilePath)){
            return $token === file_get_contents(self::$tokenFilePath);
        }

        return false;
    }

    public static function clearToken(): void
    {
        FileSystemOperations::deleteFile(self::$tokenFilePath);
    }
}